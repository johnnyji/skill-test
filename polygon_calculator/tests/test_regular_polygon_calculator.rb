require "../classes/regular_polygon_calculator"
require_relative "config"

class TestRegularPolygonCalculator < Minitest::Test

  def setup
    @calculator = RegularPolygonCalculator.new
  end

  def test_calculate_perimeter
    expected = @calculator.calculate_perimeter(4, 2)
    assert_equal(expected, 8)
  end

  def test_calculate_area
    expected = @calculator.calculate_area(3, 4)
    assert_equal(expected, 6.93)
  end

  def test_calculate_circle_area
    expected = @calculator.calculate_circle_area(5)
    assert_equal(expected, 78.54)
  end

  def test_calculate_circle_perimeter
    expected = @calculator.calculate_circle_perimeter(7)
    assert_equal(expected, 43.98)
  end
  
end