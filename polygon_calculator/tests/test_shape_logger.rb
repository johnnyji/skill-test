require "../classes/shape_logger"
require_relative "config"

class TestShapeLogger < Minitest::Test

  def setup
    @logger = ShapeLogger.new("../data/data.csv")
  end

  def test_call
    # checks if the STDOUT matches the example output provided in the tech test
    assert_output(/having a perimeter of 20 and an area of 25 units square/) { @logger.call }
    assert_output(/having a perimeter of 12.57 and an area of 12.57 units square/) { @logger.call }
  end
  
end