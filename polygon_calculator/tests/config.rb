# configuration for Minitest

require "minitest/autorun"
require "minitest/reporters"

Minitest::Reporters.use! #adds colored terminal output, because unicolor terminal output is barbaric.