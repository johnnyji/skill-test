class RegularPolygonCalculator

  def initialize
  end

  # calculate perimeter of any regular polygon
  def calculate_perimeter(num_of_sides, side_length)
    result = num_of_sides * side_length
    return_int_or_float(result)
  end

  # returns the area of any regular polygon
  def calculate_area(num_of_sides, side_length)
    perimeter = calculate_perimeter(num_of_sides, side_length)
    apothem = side_length / (2 * Math.tan(Math::PI / num_of_sides))
    result = (0.5 * perimeter * apothem).round(2)
    return_int_or_float(result)
  end

  def calculate_circle_perimeter(radius)
    result = (2 * (Math::PI * radius.to_i)).round(2)
    return_int_or_float(result)
  end

  def calculate_circle_area(radius)
    result = (Math::PI * radius**2).round(2)
    return_int_or_float(result)
  end

  private

  def return_int_or_float(result)
    result % 1 == 0 ? result.to_i : result.to_f
  end

end