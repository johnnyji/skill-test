require "csv"
require_relative "regular_polygon_calculator"

class ShapeLogger

  def initialize(csv_path)
    @csv_path = csv_path
    @calculator = RegularPolygonCalculator.new
  end

  def call
    data = CSV.read(@csv_path).to_h #reads the CSV file and converts the data to hash with shape name as key and side length as value
    data.each_with_index do |(shape, side_length), index|
      side_length = eval(side_length) #converts the side length to either float or integer
      case shape
      when "circle" then output_circle_stats(side_length, index)
      when "triangle" then output_regular_polygon_stats(shape, 3, side_length, index)
      when "square" then output_regular_polygon_stats(shape, 4, side_length, index)
      when "pentagon" then output_regular_polygon_stats(shape, 5, side_length, index)
      else puts "Sorry, I don't recognize what shape #{shape} is :("
      end  
    end

  end


  # private methods only accesible by methods with in the class itself.
  private

  def output_circle_stats(radius, index)
    output = {
      shape: "circle",
      side_type: "radius",
      side_length: radius,
      perimeter: @calculator.calculate_circle_perimeter(radius),
      area: @calculator.calculate_circle_area(radius),
      index: index + 1, #plus one so it avoids logging out 0 index
    }
    log_output(output)
  end

  def output_regular_polygon_stats(shape, num_of_sides, side_length, index)
    output = {
      shape: shape,
      side_type: "side length",
      side_length: side_length,
      perimeter: @calculator.calculate_perimeter(num_of_sides, side_length),
      area: @calculator.calculate_area(num_of_sides, side_length),
      index: index + 1, #plus one so it avoids logging out 0 index
    }
    log_output(output)
  end

  # logs out the output, the "output" argument is an object containing all the information about the shape
  def log_output(output)
    puts "Shape #{output[:index]}, is a #{output[:shape]}, with #{output[:side_type]} #{output[:side_length]}, having a perimeter of #{output[:perimeter]} and an area of #{output[:area]} units square."
  end

end