-- dogs (id, name, age)
-- bones (id, dog_id, animal_type, rating)

-- Write a SQL query on the tables to return the number of bones each dog has (including 0 if a dog has none), and the average rating of those bones.

SELECT dogs.name AS "Dog Name",
  COALESCE(COUNT(bones.id), 0) AS "Number of Bones", -- Selects the count of bones, or defaults to 0 if null
  AVG(bones.rating) AS "Average Rating", -- Selects the average rating of bones
FROM dogs LEFT OUTER JOIN bones -- Only returns the bones-(right table) that have dogs-(left table) associated
ON dogs.id = bones.dog_id -- Joins on foreign key "dog_id"
GROUP BY "Dog Name" -- Groups bones by each dog
ORDER BY "Number of Bones" DESC; -- Order from the most bones to the least


-- I didn't have enough time to fully test this problem because I approached questions 1 and 3 first. If I did have more time, I would insert some mock data into somewhere like sqlfiddle and play around more.


-- See dog.rb for ActiveRecord version