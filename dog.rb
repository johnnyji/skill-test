# Relations

class Dog < ActiveRecord::Base
  has_many :bones
end

class Bone < ActiveRecord::Base
  belongs_to :dog
end


# Query

Dog.joins(:bones).select("dogs.name AS dog_name, COALESCE(COUNT(bones.id), 0) AS bone_count, AVG(bones.rating) AS average_bone_rating").group(:name).order("bone_count DESC")

# Given more time I would read deeper into the ActiveRecord docs for querying to find ways to improve this query.

# See dog.sql for SQL version