arr = [1, 2, 3, 4, 5, 6]

def recursive_array(input, second_iteration = false)
  # only redefine the input on the first iteration, because the input will already be a hash on the second
  input = input.each_with_index.map { |item, index| [index, item] }.to_h unless second_iteration

  input.each do |k, v|
    output = ""
    k.times { output << " " }
    if second_iteration
      output << "</#{v}>"
      puts output
    else
      output << "<#{v}>"
      puts output
      recursive_array(input.to_a.reverse.to_h, true) if k == input.length - 1
      # recalling the method, but this time with key value pairs in reverse order; so that the whitespace and numbers will log out in reverse order as well, forming the bottom portion of the arrow.
    end
  end
end

recursive_array(arr)

# => expected output

# <1>
#   <2>
#     <3>
#       <4>
#         <5>
#           <6>
#           </6>
#         </5>
#       </4>
#     </3>
#   </2>
# </1>